import heapq


class Cell(object):
    def __init__(self, x, y, reachable):
        
        self.reachable = reachable
        self.x = x
        self.y = y

        self.g = 0
        self.h = 0
        self.f = 0
        
        self.parent = None

    def __lt__(self, other):
        return self.f < other.f



class AStar(object):
    def __init__(self):
        # open list
        self.opened = []
        heapq.heapify(self.opened)

        # visited cells list
        self.closed = set()

        # grid cells
        self.cells = []
        self.grid_height = None
        self.grid_width = None
        self.grid_search = 0
       

    def init_grid(self, width, height, walls, start, end):
        
        self.grid_height = height
        self.grid_width = width
        
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                if (x, y) in walls:
                    reachable = False
                else:
                    reachable = True
                self.cells.append(Cell(x, y, reachable))

        
        self.start = self.get_cell(*start)
        self.end = self.get_cell(*end)
                

    def get_heuristic(self, cell):
       
        return 10 * (abs(cell.x - self.end.x) + abs(cell.y - self.end.y))

    def get_cell(self, x, y):
        
        return self.cells[x * self.grid_height + y]

    def get_adjacent(self, cell):
        
        cells = []
        if cell.x < self.grid_width-1:
            cells.append(self.get_cell(cell.x+1, cell.y))
        if cell.y > 0:
            cells.append(self.get_cell(cell.x, cell.y-1))
        if cell.x > 0:
            cells.append(self.get_cell(cell.x-1, cell.y))
        if cell.y < self.grid_height-1:
            cells.append(self.get_cell(cell.x, cell.y+1))
        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y)]
        
        while cell.parent is not self.start:
            cell = cell.parent
            path.append((cell.x, cell.y))


        path.reverse()
        return path

    def update_cell(self, adj, cell):
        
        adj.g = cell.g + 10
        adj.h = self.get_heuristic(adj)
        adj.parent = cell
        adj.f = adj.h + adj.g

    def solve(self):
        
        heapq.heappush(self.opened, (self.start.f, self.start))
        
        while len(self.opened):
            f, cell = heapq.heappop(self.opened)
            self.closed.add(cell)
            
            if cell is self.end:
                return self.get_path()
            
            adj_cells = self.get_adjacent(cell)
            
            for adj_cell in adj_cells:
                if adj_cell.reachable and adj_cell not in self.closed:
                    if (adj_cell.f, adj_cell) in self.opened:
                       
                        if adj_cell.g > cell.g + 10:
                            self.update_cell(adj_cell, cell)
                    else:
                        self.update_cell(adj_cell, cell)
                        heapq.heappush(self.opened, (adj_cell.f, adj_cell))
                        self.grid_search = self.grid_search + 1
  
def floor_search(inputFile, outputFile):
    
    inputData = inputFile.readline()
    splitedData = inputData.split()

    floor = int(splitedData[0])
    row = int(splitedData[1])
    column = int(splitedData[2])

    grid = [[0]*column for i in range(row)]
    grid = [[int(x) for x in inputFile.readline().split()] for y in range(row)]

    wallList = []
    k = 0

    for m in range(0, row):
        for n in range(0, column):
            if grid[n][m] == 1:
                wallList.append((n, m))
                k = k + 1

            elif grid[n][m] == 3:
                startP = (n, m)

            elif grid[n][m] == 4:
                endP = (n, m)

            elif grid[n][m] == 6:
                keyP = (n, m)

    beforeKey = AStar()
    afterKey = AStar()
    beforeKey.init_grid(column, row, wallList, startP, keyP)
    afterKey.init_grid(column, row, wallList, keyP, endP)

    beforeList = beforeKey.solve()
    afterList = afterKey.solve()

    resultList = beforeList + afterList
    new = resultList

    outputFile.write("\nMatrix :\n")

    for m in range(0, row):
        for n in range(0, column):
            if (m, n) in new:
                grid[m][n] = 5
            if (m, n) == endP:
                grid[m][n] = 4
            outputFile.write((str)(grid[m][n]))
            outputFile.write(" ")
        outputFile.write("\n")
        
    outputFile.write("---\n")
    outputFile.write("length = ")
    outputFile.write((str)(len(beforeList) + len(afterList)))
    outputFile.write("\n")
    outputFile.write("time = ")
    outputFile.write((str)(beforeKey.grid_search + afterKey.grid_search - 1))
    outputFile.close()


def first_floor():
    firstFile = open("first_floor_input.txt","r")
    firstOutput = open("first_floor_output.txt", "w")
    floor_search(firstFile, firstOutput)
    

def second_floor():
    secondFile = open("second_floor_input.txt","r")
    secondOutput = open("second_floor_output.txt", "w")
    
    floor_search(secondFile, secondOutput)
    

def third_floor():
    thirdFile = open("third_floor_input.txt","r")
    thirdOutput = open("third_floor_output.txt", "w")
    
    floor_search(thirdFile, thirdOutput)
    

def fourth_floor():
    fourthFile = open("fourth_floor_input.txt","r")
    fourthOutput = open("fourth_floor_output.txt", "w")
    
    floor_search(fourthFile, fourthOutput)

    

def fifth_floor():
    fifthFile = open("fifth_floor_input.txt","r")
    fifthOutput = open("fifth_floor_output.txt", "w")
    
    floor_search(fifthFile, fifthOutput)

#====main===
    
first_floor()
second_floor()
third_floor()
fourth_floor()
fifth_floor()

